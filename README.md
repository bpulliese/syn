# README #

This README includes the steps that are necessary to get the Synapze User Interface Library up and running for development.

### What is this repository for? ###

SYN is a user interface library for all Synapze apps. The idea behind this UI library is for other developers to quickly and efficiently build application UI while maintaining all the UX and UI principles. This should then become about maintaining the UI components that makeup a Synapze application rather than maintaining the application and its individual UI components.

### How do I get set up? ###

1. Clone this repository
2. CD into the cloned repo
3. run `npm install`

### Available Npm Scripts ###
* **To build sass** - `npm run build-css`
* **To build doc sass** - `npm run build-doc-css`
* **To watch for sass changes** - `npm run watch-css`

### Install as NPM ##
(via HTTP) npm install git+https://bpulliese@bitbucket.org/bpulliese/syn.git --save

(via SSH) npm install git+ssh://bpulliese@bitbucket.org/bpulliese/syn.git --save

### How do I view the SYN documentation ###

View /doc/index.html in your preferred browser.